import "./App.css";
import React, { useState, useEffect, useContext, useReducer } from "react";
import CartasJogador from "./components/jogador/CartasJogador";
import CampoDeBatalha from "./components/campoDeBatalha/CampoDeBatalha";
import DeckJogador from "./components/jogador/DeckJogador";
import StatusJogador from "./components/jogador/StatusJogador";
import ModalEfeito from "./components/campoDeBatalha/ModalEfeito";
import AtivoModalEfeito from "./components/campoDeBatalha/AtivoModalEfeito";
import GameContexto from "./components/campoDeBatalha/GameContexto";
import reducer, { reduzirVidaPlayer1, reduzirVidaPlayer2 } from "./components/campoDeBatalha/Reducer";

import socketIOClient from "socket.io-client";
const ENDPOINT = "http://localhost:4001";

function App() {
  const [detalhes, setDetalhes] = useState({
    tituloDetalhes: "",
    descricaoDetalhes: "",
  });

  const socket = socketIOClient(ENDPOINT);

  useEffect(() => {


    socket.on('atualizaCampo', (data) => {
      console.log(data);
    });

  }, []);

  function addCartaAoCampo(idCarta, idCampo) {
    socket.emit('addCartaAoCampo', { idCarta, idCampo });
  }

  const [ativoModalEfeito, setAtivoModalEfeito] = useState(false);
  const [idEfeito, setIdEfeito] = useState(null);
  const [posicaoCartaEfeito, setPosicaoCartaEfeito] = useState(null);

  const modalEfeitoData = {
    ativoModalEfeito,
    setAtivoModalEfeito,
    idEfeito,
    setIdEfeito,
    posicaoCartaEfeito,
    setPosicaoCartaEfeito,
    reduzirVidaPlayer1,
    reduzirVidaPlayer2,
  }

  const [player1, setPlayer1] = useState({
    nome: "",
    vida: 10,
    prata: 2,
    cartas: [],
    deck: [],
    campo: [],
    mao: [],
  });

  const [player2, setPlayer2] = useState({
    nome: "",
    vida: 10,
    prata: 2,
    cartas: [],
    deck: [],
    campo: [],
    mao: [],
  });

  const initialState = {
    player1,
    player2,
    setPlayer1,
    setPlayer2,
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  const gameContexto = {
    dispatch,
  }

  return (

    <GameContexto.Provider value={gameContexto}>
      <AtivoModalEfeito.Provider value={modalEfeitoData}>
        <div>

          <div style={{ color: '#fff' }}>Vida P1: {state.player1.vida}</div>
          <div style={{ color: '#fff' }}>Prata P1: {state.player1.prata}</div>
          <br />
          <div style={{ color: '#fff' }}>Vida P2: {state.player2.vida}</div>
          <div style={{ color: '#fff' }}>Prata P2: {state.player2.prata}</div>

          <ModalEfeito titulo="O que deseja fazer?" descricao="Ganhar o Jogo Imediatamente" />
          <StatusJogador />
          <CampoDeBatalha detalhes={detalhes} setDetalhes={setDetalhes} addCartaAoCampo={addCartaAoCampo} />
          <CartasJogador setDetalhes={setDetalhes} />
          <DeckJogador />
        </div>
      </AtivoModalEfeito.Provider>
    </GameContexto.Provider>
  );
}

export default App;
