import React from "react";

export default function ImagemCarta(props) {

    const srcImagem = "./assets/img/cartas/" + props.nomeImagem;

    return (
        <img src={`${srcImagem}`} alt={props.alt} draggable="false" />
    );
}