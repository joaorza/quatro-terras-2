import React from "react";

export default function TituloCarta(props) {
    return (
        <div className="tituloCarta">
            <h2>{props.titulo}</h2>
        </div>
    );
}
