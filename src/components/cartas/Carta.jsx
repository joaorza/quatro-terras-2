import './Carta.css'
import React, { useEffect } from 'react'
import ImagemCarta from './ImagemCarta'
import DescricaoCarta from './DescricaoCarta'
import TituloCarta from './TituloCarta'
import CustoCarta from './CustoCarta'

export default function Carta(props) {

    const id = props.id
    const titulo = props.titulo
    const nomeImagem = props.nomeImagem
    const alt = props.alt
    const descricaoCarta = props.descricaoCarta
    const fraseCarta = props.fraseCarta
    const corCarta = props.corCarta || 'black'
    const custo = props.custo
    const setDetalhes = props.setDetalhes

    return (
        <div id={id} className={`Carta ${corCarta}`} draggable="true" onClick={() => setDetalhes({
            tituloDetalhes: titulo,
            descricaoDetalhes: descricaoCarta,
            corCarta: corCarta,
        })}>


            <TituloCarta titulo={titulo} />
            <ImagemCarta nomeImagem={nomeImagem} alt={alt} />
            <DescricaoCarta descricaoCarta={descricaoCarta} fraseCarta={fraseCarta} />
            <CustoCarta custo={custo} />

        </div>
    )
}