import React from "react";

export default function TituloCarta(props) {
    return (

        <div className='descricaoCarta' >
            <strong><p>{props.descricaoCarta}</p> </strong>
            <p className='italico'>{props.fraseCarta}</p>
        </div>
    );
}
