// reduzir vida do player1
export function reduzirVidaPlayer1(dispatch, vida) {
    dispatch({ type: "REDUZIR_VIDA_PLAYER1", payload: vida });
}

export function aumentarPrataPlayer1(dispatch, prata) {
    dispatch({ type: "AUMENTAR_PRATA_PLAYER1", payload: prata });
}