export function reduzirVidaPlayer2(dispatch, vida) {
    dispatch({ type: "REDUZIR_VIDA_PLAYER2", payload: vida });
}

export function aumentarPrataPlayer2(dispatch, prata) {
    dispatch({ type: "AUMENTAR_PRATA_PLAYER2", payload: prata });
}