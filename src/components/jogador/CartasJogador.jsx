import './CartasJogador.css';
import React, { useEffect } from "react";
import Carta from '../cartas/Carta'
import deck_jogador from "./deck_jogador";

export default function CartasJogador(props) {

    return (
        <div className="CartasJogador">
            {
                deck_jogador.map(carta => {
                    return (
                        <Carta key={carta.id} {...carta} setDetalhes={props.setDetalhes} />
                    )
                }
                )
            }
        </div>
    )
}