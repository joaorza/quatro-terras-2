import './DeckJogador.css'
import React from "react";

export default props => {
    return (
        <div className="DeckJogador" draggable="false">

            <img src="./assets/img/cartas/fundo_carta.jpg" draggable="false" />
            <div className="quantidadeRestante" draggable="false">35</div>

        </div>
    );
};