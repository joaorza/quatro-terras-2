import './DetalhesCarta.css';
import React from "react";

export default function DetalhesCarta(props) {

    const setDetalhes = props.setDetalhes;

    return (
        <>

            {
                props.tituloDetalhes && props.descricaoDetalhes && props.corCarta && (
                    <div className={`DetalhesCarta ${props.corCarta}`} onClick={() => setDetalhes({})}>
                        <div className="TituloCarta">{props.tituloDetalhes}</div>
                        <div className="DescricaoCarta">{props.descricaoDetalhes}</div>
                    </div>
                )
            }
        </>
    );
}