import { reduzirVidaPlayer1 } from "../actions/player1";
import { reduzirVidaPlayer2 } from "../actions/player2";

export default function reducer(state, action) {
    switch (action.type) {
        case "REDUZIR_VIDA_PLAYER1":
            return {
                ...state,
                player1: {
                    ...state.player1,
                    vida: state.player1.vida - action.payload,
                },
            };
        case "REDUZIR_VIDA_PLAYER2":
            return {
                ...state,
                player2: {
                    ...state.player2,
                    vida: state.player2.vida - action.payload,
                },
            };

        case "AUMENTAR_PRATA_PLAYER1":
            return {
                ...state,
                player1: {
                    ...state.player1,
                    prata: state.player1.prata + action.payload,
                },
            };

        case "AUMENTAR_PRATA_PLAYER2":
            return {
                ...state,
                player2: {
                    ...state.player2,
                    prata: state.player2.prata + action.payload,
                },
            };
    }
}

export { reduzirVidaPlayer1, reduzirVidaPlayer2 };