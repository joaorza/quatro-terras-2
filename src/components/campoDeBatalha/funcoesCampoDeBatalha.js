import React from "react";

function dragstart() {
    this.classList.add("dragging");
}

function dragend() {
    this.classList.remove("dragging");
}

function dragover(e) {
    e.preventDefault();
    const carta = document.querySelector(".dragging");
}

function dragenter(e) {
    this.classList.add("over");
}

function dragleave() {
    this.classList.remove("over");
}

function mudaPosicaoCarta(e) {
    e.preventDefault();

    const imgPosicaoCarta = this.childNodes[1];

    if (imgPosicaoCarta.classList.contains("ataque")) {
        imgPosicaoCarta.classList.remove("ataque");
        imgPosicaoCarta.classList.add("defesa");
        imgPosicaoCarta.src = "./assets/img/cartas/escudo.png";
    } else {
        imgPosicaoCarta.classList.remove("defesa");
        imgPosicaoCarta.classList.add("ataque");
        imgPosicaoCarta.src = "./assets/img/cartas/espada.png";
    }
}

export { dragstart, dragend, dragover, dragenter, dragleave, mudaPosicaoCarta };