import './ModalEfeito.css';
import React, { useContext, useState } from 'react'
import AtivoModalEfeito from './AtivoModalEfeito'
import deck_jogador from '../jogador/deck_jogador';
import GameContexto from './GameContexto';

export default function ModalEfeito(props) {

    const contextoEfeito = useContext(AtivoModalEfeito);
    const game = useContext(GameContexto);

    function fetcharModalEfeito() {
        contextoEfeito.setAtivoModalEfeito(false);
    }

    function atacar() {
        game.dispatch({
            type: 'REDUZIR_VIDA_PLAYER2',
            payload: 1
        })

        fetcharModalEfeito()
    }

    function ativarEfeito() {
        try {
            const idCarta = contextoEfeito.idEfeito;
            const cartaSelecionada = deck_jogador.find(carta => carta.id == idCarta)

            const posicaoCarta = contextoEfeito.posicaoCartaEfeito

            let efeito;
            let payload;

            posicaoCarta == 'ataque' ? efeito = cartaSelecionada.efeitoAtaque : efeito = cartaSelecionada.efeitoDefesa
            posicaoCarta == 'ataque' ? payload = cartaSelecionada.payloadAtaque : payload = cartaSelecionada.payloadDefesa

            console.log({
                efeito,
                payload
            });
            game.dispatch({
                type: efeito,
                payload: payload
            })

            fetcharModalEfeito()
        }
        catch (e) {

        }
    }

    return (
        <>
            {contextoEfeito.ativoModalEfeito &&
                <div>
                    <div className="backgroundModal"></div>
                    <div className="modal">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h2>{props.titulo}</h2>
                                <span className="close" onClick={() => contextoEfeito.setAtivoModalEfeito(false)}>&times;</span>
                            </div>
                            <div className="modal-body">
                                <p>{props.descricao}</p>
                                <p>Id: {contextoEfeito.idEfeito}</p>

                            </div>

                            <div className="modal-footer">
                                <button className="btn btn-success" onClick={() => atacar()}>Atacar</button>
                                <button className="btn btn-purple" onClick={() => ativarEfeito()}>Ativar Efeito</button>
                                <button className="btn btn-danger" onClick={() => contextoEfeito.setAtivoModalEfeito(false)}>Fechar</button>
                            </div>
                        </div>
                    </div>
                </div>
            }
        </>
    )
}