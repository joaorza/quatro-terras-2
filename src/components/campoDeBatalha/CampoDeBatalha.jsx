import './CampoDeBatalha.css';
import React, { useEffect, useState, useContext } from "react";
import {
    dragstart,
    dragend,
    dragover,
    dragenter,
    dragleave,
    mudaPosicaoCarta,
} from './funcoesCampoDeBatalha';

import AtivoModalEfeito from './AtivoModalEfeito';
import DetalhesCarta from './DetalhesCarta';

export default function CampoDeBatalha(props) {
    const addCartaAoCampo = props.addCartaAoCampo;
    useEffect(() => {

        const cartas = document.querySelectorAll(".Carta");
        cartas.forEach((carta) => {
            carta.addEventListener("dragstart", dragstart);
            carta.addEventListener("dragend", dragend);
        });

        const cartasCampoDeBatalha = document.querySelectorAll(".CartaCampoBatalha");
        cartasCampoDeBatalha.forEach((carta) => {
            carta.addEventListener("dragover", dragover);
            carta.addEventListener("dragenter", dragenter);
            carta.addEventListener("dragleave", dragleave);
            carta.addEventListener("drop", drop);
            carta.addEventListener("contextmenu", mudaPosicaoCarta);
            carta.addEventListener("dblclick", modalAtivarEfeito);
        });


        function drop() {
            this.classList.remove("over");
            const carta = document.querySelector(".dragging");

            if (this.innerText === "") {
                this.appendChild(carta);
                this.childNodes[0].draggable = false;

                const idCarta = this.childNodes[0].id;
                const idCampo = this.id;

                addCartaAoCampo(idCarta, idCampo);

                const img = document.createElement("img");
                img.src = "./assets/img/cartas/espada.png";
                img.classList.add("ImgTipoPosicaoCarta");
                img.classList.add("ataque");

                this.appendChild(img);
            }
        }

        function modalAtivarEfeito() {
            contextoEfeito.setAtivoModalEfeito(true);

            let [carta, posicaoCarta] = this.childNodes

            carta = this.childNodes[0].id;
            posicaoCarta = posicaoCarta.classList[1];

            contextoEfeito.setIdEfeito(carta);
            contextoEfeito.setPosicaoCartaEfeito(posicaoCarta);

        }

    }, []);

    const detalhes = props.detalhes;
    const setDetalhes = props.setDetalhes;

    const contextoEfeito = useContext(AtivoModalEfeito);

    return (
        <div style={{ perspective: '100px' }}>

            <DetalhesCarta {...detalhes} setDetalhes={setDetalhes} />

            <div className="CampoDeBatalha">

                <div className="AreaBatalha">
                    <div id="p2-1" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p2-2" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p2-3" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p2-4" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p2-5" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p2-6" className="CartaCampoBatalha" draggable="false"></div>
                </div>

                <div className="AreaBatalha ">
                    <div id="p1-1" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p1-2" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p1-3" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p1-4" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p1-5" className="CartaCampoBatalha" draggable="false"></div>
                    <div id="p1-6" className="CartaCampoBatalha" draggable="false"></div>
                </div>

            </div>


        </div>

    );
}