export default () => {
    const cartas = document.querySelectorAll(".Carta");
    cartas.forEach((carta) => {
        carta.addEventListener("dragstart", dragstart);
        carta.addEventListener("dragend", dragend);
    });

    function dragstart() {
        this.classList.add("dragging");
    }

    function dragend() {
        this.classList.remove("dragging");
    }

    const cartasCampoDeBatalha = document.querySelectorAll(".CartaCampoBatalha");
    cartasCampoDeBatalha.forEach((carta) => {
        carta.addEventListener("dragover", dragover);
        carta.addEventListener("dragenter", dragenter);
        carta.addEventListener("dragleave", dragleave);
        carta.addEventListener("drop", drop);
    });

    function dragover(e) {
        e.preventDefault();
        const carta = document.querySelector(".dragging");
    }

    function dragenter(e) {
        this.classList.add("over");
    }

    function dragleave() {
        this.classList.remove("over");
    }

    function drop() {
        this.classList.remove("over");
        const carta = document.querySelector(".dragging");
        if (this.innerText === "") {
            this.appendChild(carta);
            this.childNodes[0].draggable = false;
        }
    }
};