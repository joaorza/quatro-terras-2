const express = require("express");
const app = express();

var cors = require("cors");

app.use(cors());

const http = require("http");
const server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server, {
    cors: {
        origin: "*",
        methods: "GET,HEAD,PUT,PATCH,POST,DELETE",
        allowedHeaders: "Content-Type, Authorization, X-Requested-With, X-Socket-ID",
        credentials: true,
    },
});

app.get("/", function(req, res) {
    res.send({ response: "oi" });
});

io.on("connection", (socket) => {
    console.log("connected");
    socket.on("disconnect", () => console.log("disconnected"));

    socket.on("addCartaAoCampo", (cartas) => {
        io.emit("atualizaCampo", cartas);
    });
});

server.listen(4001, function() {
    console.log("Listening on port 4001");
});